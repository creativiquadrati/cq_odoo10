# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import datetime

from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare
from odoo.exceptions import UserError

class Picking(models.Model):
    _inherit = "stock.picking"
    
    def _create_lots_for_picking(self):
        super(Picking, self)._create_lots_for_picking()
        for pack_op_lot in self.mapped('pack_operation_ids').mapped('pack_lot_ids'):
            if pack_op_lot.lot_id and pack_op_lot.removal_date_in:
                rem_date = pack_op_lot.removal_date_in
                lot = pack_op_lot.lot_id
                res = { 'life_date': rem_date,
                        'use_date': rem_date,
                        'removal_date': rem_date,
                        'alert_date': rem_date
                       }
                duration = lot.product_id.removal_time
                if duration:
                    date_start = datetime.datetime.strptime(rem_date, DEFAULT_SERVER_DATETIME_FORMAT) - datetime.timedelta(days=duration)
                    res.update(lot._get_dates(date_start=date_start))
                lot.write(res)
    create_lots_for_picking = _create_lots_for_picking
